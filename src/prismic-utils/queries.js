import gql from 'graphql-tag';

export const homepageQuery = gql`
  query{
    homepage(uid: "homepage", lang: "en-ca"){
      title_of_homepage
    }
  }
`;

export const aboutUsQuery = gql`
  query{
    about_us(uid:"1", lang: "en-ca"){
      image
      about_text
    }
  }
`;

export const collectionsQuery = gql`
query {
  allProducts{
    edges {
      node{
        title
        image
        description
        featured
        _meta {
          uid
        }
        _linkType
        variants {
          variant_text
          variant_image
        }
      }
    }
  }
}`;

export const featuredProductsQuery = gql`
query {
  allProducts(where: {featured: true}){
    edges {
      node{
        title
        image
        description
        featured
        _meta {
          uid
        }
        _linkType
        variants {
          variant_text
          variant_image
        }
      }
    }
  }
  featured_products(uid: "1", lang: "en-ca") {
    title
    description
  }
}
`;

export const productQuery = gql`
query Product($uid: String!) {
  product(uid: $uid, lang: "en-ca") {
    title
    image
    description
    price
  }
}
`;
