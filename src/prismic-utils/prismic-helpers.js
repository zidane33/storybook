import { useState, useEffect } from 'react';
import { client } from '../prismic-configuration';

export const fetchDataWithParams = async (query, variables) => {
  const result = await client.query({
    query,
    variables: variables || null,
  });
  return result;
};

const useFetch = (query, variablesObject) => {
  const [doc, setDocData] = useState(null);
  const [notFound, toggleNotFound] = useState(false);

  useEffect(() => {
    const loadData = async () => {
      const result = await fetchDataWithParams(query, variablesObject);
      console.log(result);

      if (result) {
        return setDocData(result);
      }
      console.warn('Page document not found. Make sure it exists in your Prismic repository');
      toggleNotFound(true);
    };
    loadData();
  }, []);

  return { doc, notFound };
};

export default useFetch;
