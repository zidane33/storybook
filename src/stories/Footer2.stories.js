// YourComponent.stories.js

import React from "react";
import Footer from "../components/footer/Footer";

// This default export determines where your story goes in the story list
export default {
  title: "Footer",
  component: Footer,
};

const Template = (args) => <Footer {...args} />;

export const primary = Template.bind({});

primary.args = {
  /* the args you need here will depend on your component */
  backgroundColor: "red",
};
