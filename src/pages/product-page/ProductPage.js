import React from 'react';
import { RichText } from 'prismic-reactjs';
import NotFound from '../NotFound';
import useFetch from '../../prismic-utils/prismic-helpers';
import { productQuery } from '../../prismic-utils/queries';
import { linkResolver } from '../../prismic-configuration';
import './product-page.scss';

const ProductPage = ({ match }) => {
  const { uid } = match.params;
  const { doc, notFound } = useFetch(productQuery, { uid });

  if (doc) {
    const {
      image, description, title, price,
    } = doc.data.product;
    return (
      <div className="product-container">
        <div className="image-container">
          <img src={image.url} />
        </div>
        <div className="text-container">
          <h1 className="product-title">{title[0].text}</h1>
          <div className="product-description">{RichText.render(description, linkResolver)}</div>
          <strong className="product-price">${price}</strong>
          <button className="add-cart-button">Add to Cart</button>
        </div>
      </div>
    );
  } if (notFound) {
    return <NotFound />;
  }
  return null;
};

export default ProductPage;
