import React from 'react';
import NotFound from '../NotFound';
import useFetch from '../../prismic-utils/prismic-helpers';
import { collectionsQuery } from '../../prismic-utils/queries';

const Collections = () => {
  const { doc, notFound } = useFetch(collectionsQuery);

  if (doc) {
    const { edges } = doc.data.allProducts;
    return (
      <div className="page">
        {edges.map((product, idx) => <h1 key={idx}>{product.node.title[0].text}</h1>)}
      </div>
    );
  } if (notFound) {
    return <NotFound />;
  }
  return null;
};

export default Collections;
