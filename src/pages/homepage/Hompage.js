import React from 'react';
import NotFound from '../NotFound';
import useFetch from '../../prismic-utils/prismic-helpers';
import { homepageQuery } from '../../prismic-utils/queries';
import FeaturedProducts from '../../components/featured-products/FeaturedProducts';

const Homepage = () => {
  const { doc, notFound } = useFetch(homepageQuery);

  if (doc) {
    return (
      <div>
        <FeaturedProducts />
      </div>
    );
  } if (notFound) {
    return <NotFound />;
  }
  return null;
};

export default Homepage;
