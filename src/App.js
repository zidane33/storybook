import React, { Fragment } from "react";
import { Helmet } from "react-helmet";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Help, NotFound } from "./pages";
import Collections from "./pages/collections/collections";
import Homepage from "./pages/homepage/Hompage";
import Nav from "./components/nav/Nav";
import Footer from "./components/footer/Footer";
import ProductPage from "./pages/product-page/ProductPage";

/**
 * Main application componenet
 */
const App = () => (
  <Fragment>
    <Helmet></Helmet>
    <BrowserRouter>
      <Nav />
      <Switch>
        <Route exact path="/" component={Homepage} />
        <Route exact path="/help" component={Help} />
        <Route exact path="/collections" component={Collections} />
        <Route exact path="/product/:uid" component={ProductPage} />
        <Route component={NotFound} />
      </Switch>
      <Footer backgroundColor="black" />
    </BrowserRouter>
  </Fragment>
);

export default App;
