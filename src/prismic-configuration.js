import { PrismicLink } from 'apollo-link-prismic';
import { InMemoryCache } from 'apollo-cache-inmemory';
import ApolloClient from 'apollo-client';

// -- Prismic API endpoint
// Determines which repository to query and fetch data from
export const apiEndpoint = 'https://littlerocket.cdn.prismic.io/api/v2';
export const graphqlEndpoint = 'https://littlerocket.prismic.io/graphql';

// -- Link resolution rules
// Manages the url links to internal Prismic documents
export const linkResolver = (doc) => {
  if (doc.type === 'product') return `/product/${doc.uid}`;
  return '/';
};

// Client method to query documents from the Prismic repo
export const client = new ApolloClient({
  link: PrismicLink({
    uri: graphqlEndpoint,
    accessToken: '',
    repositoryName: 'dummy-repo',
  }),
  cache: new InMemoryCache(),
});
