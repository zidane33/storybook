import React from 'react';
import { Link } from 'react-router-dom';
import { NotFound } from '../../pages';
import useFetch from '../../prismic-utils/prismic-helpers';
import { featuredProductsQuery } from '../../prismic-utils/queries';
import './featured-products.scss';

const FeaturedProducts = () => {
  const { doc, notFound } = useFetch(featuredProductsQuery);

  if (doc) {
    const { featured_products } = doc.data;
    return (
      <div className="featured-products-container">
          <h1 className="featured-products-title">{featured_products.title[0].text}</h1>
          <p className="featured-products-description">{featured_products.description}</p>
          <Link className="shop-all-button" to="/collections">Shop All</Link>
      </div>
    );
  } if (notFound) {
    return <NotFound />;
  }
  return null;
};

export default FeaturedProducts;
