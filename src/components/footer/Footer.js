import React from "react";
import "./footer.scss";

const Footer = ({ backgroundColor }) => (
  <footer style={{ backgroundColor }} className="footer-container">
    <p className="footer-text"> Developed by Little Rocket </p>
  </footer>
);

export default Footer;
