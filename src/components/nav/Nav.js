import React from 'react';
import { Link } from 'react-router-dom';
import './nav.scss';

const Nav = () => (
  <>
    <ul className="navbar">
      <li className="nav-block">
        <Link to="/">
          Home
        </Link>
      </li>
      <li className="nav-block">
        <Link to="/about-us">
          About
        </Link>
      </li>
      <li className="nav-block">
        <Link to="/collections">
          Shop
        </Link>
      </li>
    </ul>
  </>
);

export default Nav;
